# Kantar Sample Test

This project is a Kantar test, where we have written BDD tests for comparing two test text files. 

### Framework/Project structure

BDD Framework with below project structure and is done using Java language.

```bash
utilities package at below location having all the utility files.
src/main/java/utilities

base package at the below location contains BaseClass.class to run the tests.
src/test/java/base

runner package at the below location contains TestRunner.class to run the tests.
src/test/java/runner

stepDefinitions package at the below location contains step definition files.
src/test/java/stepDefinitions

Features folder contains all the feature files at below location
src/test/resources/Features

TestData folder contains all the test data files at below location
src/test/resources/TestData

log4j.properties file is used to configure logs output.
```

### Requirements to Run the Tests

* Java should be installed.
* Maven should be setup.
* Running Unix based commands on windows will require apps like Cygwin or Git Bash.
* System Environment variables for above are properly configured.

 
### How to Run Tests

Run via Eclipse IDE.
* Open the project home directory.
* Right-click on pom.xml and select Run As 'Maven install'

Run via bat file.

* Open command prompt/terminal.
* Open the project home directory.
* type run.bat and press enter.

Run via bash script file.

* Open command prompt/terminal.
* Open the project home directory.
* type .\run.sh and press enter.


### Test Report
Once the Test has been run the Test Reports can be seen at below
* Under <project home directory>/target/cucumber.html. This is an HTML file, so please open it on a browser.


### Logs
Once the Test has been run the Logs are generated at below locations
* If Tests are run on Eclipse IDE, then the console has logs that are properly marked for Success and Failure cases. Also, it will show a summary of the results at the end.
* Log file will be generated at this location <project home directory>/log/testlog.log.

* Logs with only summary and Error Logs can also be found in the file <project home directory>/target/surefire-reports/runner.TestRunner.txt.

* If Tests are run on command Line/Terminal, then the same logs will appear on the terminal itself with a brief summary of the result.



### Assumptions
* Maven project is used, JUnit, cucumber, etc minimal to minimal Jar files are used for this project as told.
* Basic report is generated, the extent report is not used.

<br />
<br />
<br />
<br />
