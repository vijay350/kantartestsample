package base;

import java.util.HashMap;
import org.apache.log4j.Logger;

public class BaseClass {

	public static Logger logger;

	public final static String inputFilePath = "./src/test/resources/TestData/be0008_pre_change";
	public final static String outputFilePath = "./src/test/resources/TestData/be0008_post_change";
	public static HashMap<Integer, String> newInputHM = new HashMap<Integer, String>();
	public static HashMap<Integer, String> newOutputHM = new HashMap<Integer, String>();
	
}
