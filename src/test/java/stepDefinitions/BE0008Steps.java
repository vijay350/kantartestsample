package stepDefinitions;

import java.io.FileNotFoundException;
import utilities.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;

import base.BaseClass;

public class BE0008Steps extends BaseClass{

	String verifyOutputCondition;

	@Before
	public void setup()
	{
		logger = Logger.getLogger(BE0008Steps.class.getName());
		PropertyConfigurator.configure("log4j.properties");
	}
	
	@Given("input statement contains Broadcast Date that is greater than or Equal to Viewing Date")
	public void input_broadcast_date_greaterOrEqual_than_viewing_date() throws FileNotFoundException {
		logger.info("***Starting - input statement contains Broadcast Date that is greater than or Equal to Viewing Date ***" );
		newInputHM = ReadFile.convertToHashMap(inputFilePath);
		logger.info(" Input file has been read and converted to HashMap");
		/* File converted to HashMap and send with 'Broadcast date greater than or Equal to Viewing date' condition.
		 * so that subset of the full file will be received which will only contain statements where Broadcast date 
		 * will be greater than or Equal to Viewing date.
		 */
		verifyOutputCondition = "bDGreaterEqThanVD";

		newInputHM = ReadFile.alteredHM(newInputHM, verifyOutputCondition);
		logger.info(" Input file HashMap has been altered to have only condition statified statements ");
		logger.info("***Ended - input statement contains Broadcast Date that is greater than or Equal to Viewing Date *** ");
	}

	@When("statement is converted")
	public void statement_is_converted() throws FileNotFoundException {
		/* Output File path is send so that whole output file is converted to hashmap.
		 */
		logger.info("***Starting - statement is converted ***" );
		newOutputHM = ReadFile.convertToHashMap(outputFilePath);
		logger.info(" Output file has been read and converted to HashMap");
		logger.info("***Ended - statement is converted ***" );
	}

	@Then("nothing should be changed in the output statement")
	public void nothing_should_be_changed_in_the_output_statement() {
		/* Output File path is verified with input file statements.
		 */
		logger.info("***Starting - nothing should be changed in the output statement ***" );
		if(verifyOutputCondition.equals("bDGreaterEqThanVD")) {
			for(int key:newInputHM.keySet()) {
				Assert.assertTrue(newInputHM.get(key).contentEquals(newOutputHM.get(key)));
			}
		}
		else if(verifyOutputCondition.equals("04")){
			for(int key:newInputHM.keySet()){
				Assert.assertTrue(newInputHM.get(key).contentEquals(newOutputHM.get(key)));
			}
		}
		logger.info("***Ended - nothing should be changed in the output statement ***" );
	}

	@Given("input statement contains Viewing Activity not equal to {string}")
	public void input_statement_contains_viewing_activity_not_equal_to(String viewActivityCode) throws FileNotFoundException {
		logger.info("***Starting - input statement contains Viewing Activity not equal to 04 ***" );
		
		newInputHM = ReadFile.convertToHashMap(inputFilePath);
		logger.info(" Input file has been read and converted to HashMap");
		/* Input File converted to HashMap and send with Viewing Activity 04 condition.
		 * So that subset of the full file will be received which will only contain statements where 
		 * Viewing Activity is 04.
		 */
		verifyOutputCondition = viewActivityCode;
		newInputHM = ReadFile.alteredHM(newInputHM, viewActivityCode);
		logger.info(" Input file HashMap has been altered to have only condition statified statements ");
		logger.info("***Ended - input statement contains Viewing Activity not equal to 04 ***" );
	}

	@Given("input statement contains Broadcast Date which is less than Viewing Date and Viewing Activity is {string}")
	public void input_broadcast_date_is_less_than_viewing_date_and_viewing_activity_is(String viewActivityCode) throws FileNotFoundException {
		logger.info("***Starting - input statement contains Broadcast Date which is less than Viewing Date and Viewing Activity is 04 ***" );
		
		newInputHM = ReadFile.convertToHashMap(inputFilePath);
		logger.info(" Input file has been read and converted to HashMap");
		/* File converted to HashMap and send with 'Broadcast date Less than Viewing date' and Viewing activity 04 condition.
		 * so that subset of the full file will be received which will only contain statements with specified condition.
		 */
		verifyOutputCondition = "bDLessThanVD_and_VD_4";
		newInputHM = ReadFile.alteredHM(newInputHM, verifyOutputCondition);
		logger.info("***Ended - input statement contains Broadcast Date which is less than Viewing Date and Viewing Activity is 04 ***" );
	}

	@Then("Viewing Activity changed to {string} in the output statement")
	public void viewing_activity_changed_to_in_the_output_statement(String newViewActivityCode) {
		logger.info("***Starting - Viewing Activity changed to 16 in the output statement" );
		String outputLine, outputViewingActivity;
		/* Output file is verified with corresponding input file statements where Activity code is 04.
		 */
		logger.info("Verifying input and output files statements " );
		for(int key:newInputHM.keySet()) {
			outputLine = newOutputHM.get(key).toString();
			outputViewingActivity = outputLine.substring(59, 61);
			if(!(outputViewingActivity.equals(newViewActivityCode))) {
				logger.error("***ERORR*** Output File statement "+key+" is problematic as it doesn't have new Viewing Activity Code "+newViewActivityCode);
			}
			Assert.assertTrue("***ERORR*** Output File statement "+key+" is problematic as it doesn't have new Viewing Activity Code "+newViewActivityCode, outputViewingActivity.equals(newViewActivityCode));
		}	
		logger.info("***Ended - Viewing Activity changed to 16 in the output statement ***" );
	}

	@Then("only Viewing Activity should be changed to {string} in the output statement")
	public void only_viewing_activity_should_be_changed_to_in_the_output_statement(String string) {
		logger.info("***Starting - only Viewing Activity should be changed to 16 in the output statement***" );
		String outputLine, inputLine;
		/* Output file is verified with corresponding input file statements where Activity code is 04 and Viewing Date is 
		 * greater than the Broadcast Date, so that rest of the statments except Viewing Activity codes remains unchanged.
		 */
		logger.info("Verifying input and output files statements " );
		for(int key:newInputHM.keySet()) {
			inputLine = newInputHM.get(key).toString();
			outputLine = newOutputHM.get(key).toString();
			String preVAinputFile = inputLine.substring(0,59);
			String postVAinputFile = inputLine.substring(61, 203);
			String preVAoutputFile = outputLine.substring(0,59);
			String postVAoutputFile = outputLine.substring(61, 203);
			if(!((preVAinputFile.equals(preVAoutputFile) && postVAinputFile.equals(postVAoutputFile)))){
				logger.error("***ERORR*** Output File statement "+key+" is problematic as it doesn't match with Input");
					}
			Assert.assertTrue("***ERORR*** Output File statement "+key+" is problematic as it doesn't match with Input", (preVAinputFile.equals(preVAoutputFile) && postVAinputFile.equals(postVAoutputFile)));
		}
		logger.info("***Ended - only Viewing Activity should be changed to 16 in the output statement***" );
	}

	@Then("the session duration value should not change in the output statement")
	public void the_session_duration_value_should_not_change_in_the_output_statement() {
		logger.info("***Starting - the session duration value should not change in the output statement***" );
		String outputLine, inputLine;
		/* Output file is verified with corresponding input file statements where Activity code is 04 and Viewing Date is 
		 * greater than the Broadcast Date, so that Session duration field remains unchanged.
		 */
		logger.info("Verifying input and output files statements " );
		for(int key:newInputHM.keySet()) {
			inputLine = newInputHM.get(key).toString();
			outputLine = newOutputHM.get(key).toString();
			String sessionDurationInputFile = inputLine.substring(35, 40);
			String sessionDurationOutputFile = outputLine.substring(35, 40);
			Assert.assertTrue("***ERORR*** Output File statement "+key+" is problematic as it doesn't match Session Duration Field", (sessionDurationInputFile.equals(sessionDurationOutputFile)));
		}
		logger.info("***Ended - the session duration value should not change in the output statement***" );
	}
}
