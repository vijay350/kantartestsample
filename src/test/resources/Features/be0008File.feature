#Author: vijay.nemodrg@gmail.com

Feature: To verify the content of the BE0008 output file
 
Scenario: Nothing changes when Broadcast Date is greater than or Equal to Viewing Date
	Given input statement contains Broadcast Date that is greater than or Equal to Viewing Date
  When statement is converted
  Then nothing should be changed in the output statement

Scenario: Nothing changes when Viewing Activity is not equal to '04'
  Given input statement contains Viewing Activity not equal to '04'
  When statement is converted
  Then nothing should be changed in the output statement
 
Scenario: Viewing Activity changes from '04' to '16' when Broadcast Date less than Viewing Date and Viewing Activity is '04'
  Given input statement contains Broadcast Date which is less than Viewing Date and Viewing Activity is '04'
  When statement is converted
  Then Viewing Activity changed to '16' in the output statement
  
Scenario: No other changes in the statement except to Viewing Activity when Broadcast Date less than Viewing Date and Viewing Activity is '04'
  Given input statement contains Broadcast Date which is less than Viewing Date and Viewing Activity is '04'
  When statement is converted
  Then only Viewing Activity should be changed to '16' in the output statement
  And the session duration value should not change in the output statement