package utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 * This ReadFile class contains methods for reading the input and output files, converting files
 * to HashMaps and changing to required data set for testing
 * 
 */
public class ReadFile {

	static Scanner scan;
	static HashMap<Integer, String> fileHM = new HashMap<Integer, String>();
	static HashMap<Integer, String> altHM = new HashMap<Integer, String>();

	/**
	 * This readFile method is there to read the input and output text file.
	 * @param filePath This is the first parameter which takes the path of the required text file
	 * @return scanFile This returns a Scanner object which will be used to further scan the file.
	 */
	public static Scanner readFile(String filePath) throws FileNotFoundException {
		File file = new File(filePath);
		Scanner scanFile = new Scanner(file);
		return scanFile;
	}

	/**
	 * This convertToHashMap method is there to scan the input and output text file and
	 * convert them to HashMap.
	 * @param path This is the first parameter which takes the path of the required text file.
	 * @return fileHM This returns a HashMap which was created after the text files were read.
	 */
	public static HashMap convertToHashMap(String path) throws FileNotFoundException {

		scan = ReadFile.readFile(path);
		String scanLine;
		int rownum=1;
		while(scan.hasNextLine()){
			scanLine = scan.nextLine();
			fileHM.put(rownum, scanLine);
			rownum++;
		}

		return fileHM;
	}

	/**
	 * This method is used to change the raw HashMap into required data sets HashMaps.
	 * @param hm This is the first parameter to take the raw HashMap
	 * @param condition  This is the second parameter which contains the condition by which the
	 * required data sets altered hashmaps will be created.
	 * @return HashMap This returns altered Hashmap which contains subset of the data for testing.
	 */
	public static HashMap alteredHM(HashMap hm, String condition) throws FileNotFoundException {

		// This is to clear the hashmap so that any previous data in the hashmap is flushed.
		altHM.clear();

		// This is to create an iterator to traverse through the hashmap
		Iterator hmIterator = hm.entrySet().iterator();

		// These variable are taken to store the 'Viewing Date', 'BroadCast Date' and 'Viewing Activiy'
		int inputViewingDate;
		int inputBroadCastDate;
		String inputViewingActivity;
		int rownum =1;
		while (hmIterator.hasNext()) { 
			Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
			String data = (String) mapElement.getValue();
			inputViewingDate = Integer.parseInt(data.substring(13, 18));
			inputBroadCastDate = Integer.parseInt(data.substring(24, 29));
			inputViewingActivity = data.substring(59, 61);

			if(condition.equals("bDGreaterEqThanVD")) {
				if(inputBroadCastDate >= inputViewingDate) {
					altHM.put(rownum, data);
				}
			}
			else if(condition.equals("bDLessThanVD_and_VD_4")) {
				if((inputViewingDate > inputBroadCastDate) && ((inputViewingActivity.equals("04")))) {
					altHM.put(rownum, data);
				}
			}
			else if(condition.equals("04")) {
				if(!(inputViewingActivity.equals("04"))) {
					altHM.put(rownum, data);
				}
			}

			rownum++;
		}
		return altHM; 

	}
}

